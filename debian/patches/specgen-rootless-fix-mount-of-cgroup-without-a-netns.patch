From: Giuseppe Scrivano <gscrivan@redhat.com>
Date: Thu, 21 Sep 2023 13:16:44 +0200
Subject: specgen, rootless: fix mount of cgroup without a netns

commit cf364703fc3f94cd759cc683e3ab9083e8ecc324 changed the way
/sys/fs/cgroup is mounted when there is not a netns and it now honors
the ro flag.  The mount was created using a bind mount that is a
problem when using a cgroup namespace, fix that by mounting a fresh
cgroup file system.

Closes: https://github.com/containers/podman/issues/20073

Signed-off-by: Giuseppe Scrivano <gscrivan@redhat.com>
---
 pkg/specgen/generate/oci_linux.go | 5 +++--
 test/system/030-run.bats          | 4 ++++
 2 files changed, 7 insertions(+), 2 deletions(-)

--- a/pkg/specgen/generate/oci_linux.go
+++ b/pkg/specgen/generate/oci_linux.go
@@ -113,11 +113,12 @@ func SpecGenToOCI(ctx context.Context, s *specgen.SpecGenerator, rt *libpod.Runt
 		}
 		g.AddMount(sysMnt)
 		g.RemoveMount("/sys/fs/cgroup")
+
 		sysFsCgroupMnt := spec.Mount{
 			Destination: "/sys/fs/cgroup",
-			Type:        define.TypeBind,
+			Type:        "cgroup",
 			Source:      "/sys/fs/cgroup",
-			Options:     []string{"rprivate", "nosuid", "noexec", "nodev", r, "rbind"},
+			Options:     []string{"rprivate", "nosuid", "noexec", "nodev", r},
 		}
 		g.AddMount(sysFsCgroupMnt)
 		if !s.Privileged && isRootless {
--- a/test/system/030-run.bats
+++ b/test/system/030-run.bats
@@ -1116,6 +1116,10 @@ EOF
         # verify that the last /sys/fs/cgroup mount is read-only
         run_podman run --net=host --cgroupns=host --rm $IMAGE sh -c "grep ' / /sys/fs/cgroup ' /proc/self/mountinfo | tail -n 1"
         assert "$output" =~ "/sys/fs/cgroup ro"
+
+        # verify that it works also with a cgroupns
+        run_podman run --net=host --cgroupns=private --rm $IMAGE sh -c "grep ' / /sys/fs/cgroup ' /proc/self/mountinfo | tail -n 1"
+        assert "$output" =~ "/sys/fs/cgroup ro"
     fi
 }
 
